import matplotlib.pyplot as plt
import numpy as np
import h5py
from pathlib import Path
import os

from globb import Global
from matplotlib import rcParams

import math

class EvolutionStatistics(object):

    def __init__(self, path):
        self.path = path # path to hdf5 files

        self.record_of_evolution = None # multidimensional numpy array capture selected moment oef evolution
        self.record_of_evolution_succeed = None # multidimensional numpy array with succeed flags to record_of_evolution
        self.graph_ticks = None
        self.num_succeed = None

        self.num_generations = None
        self.popsize = None
        self.num_independen_runs = None
        self.num_evaluations_per_generation = None
        self.num_of_records = None
        self.evolution_algorithm = None
        self.evolution_algorithm_settings = None
        self.other_settings = None

        # attributes about creating record of evolution
        self.rank = None

    def create_record_of_evolution(self, rank, num_generations, popsize, num_independen_runs, num_evaluations_per_generation, num_of_records, evolution_algorithm, evolution_algorithm_settings, other_settings):
        self.rank = rank

        with h5py.File("%s/rec%s.hdf5" % (self.path, self.rank), mode='w') as f:
            f.attrs['num_generations'] = num_generations
            f.attrs['popsize'] = popsize
            f.attrs['num_independen_runs'] = num_independen_runs
            f.attrs['num_evaluations_per_generation'] = num_evaluations_per_generation
            f.attrs['num_of_records'] = num_of_records
            f.attrs['evolution_algorithm'] = evolution_algorithm
            f.attrs['evolution_algorithm_settings'] = str(evolution_algorithm_settings)
            f.attrs['other_settings'] = str(other_settings)

    def append_data_to_record(self, fitness_values, succeeded, generation, succeed_individuals):
        with h5py.File("%s/rec%s.hdf5" % (self.path, self.rank), mode='a') as f:
            dset = f.create_dataset("gen_%s" % generation, data=np.asarray(fitness_values, dtype='f'))
            dset.attrs['suceeded'] = 1 if (succeeded) else 0
            f.create_dataset("gen_succeed_%s" % generation, data=np.asarray(succeed_individuals, dtype=bool))

    def load_record_of_evolution(self):
        self.num_independen_runs = self._detect_num_of_independent_runs()
        for i in range(self.num_independen_runs):
            #print(str(i))
            with h5py.File("%s/rec%s.hdf5" % (self.path, i), mode='r+') as f:

                # only from first subrecord we obtain information about evolution
                if (i == 0):
                    self.num_generations = f.attrs['num_generations']
                    self.popsize = f.attrs['popsize']
                    self.num_independen_runs = f.attrs['num_independen_runs']
                    self.num_evaluations_per_generation = f.attrs['num_evaluations_per_generation']
                    self.num_of_records = f.attrs['num_of_records']
                    self.evolution_algorithm = f.attrs['evolution_algorithm']
                    self.evolution_algorithm_settings = f.attrs['evolution_algorithm_settings']
                    self.other_settings = f.attrs['other_settings']

                    self.record_of_evolution = np.zeros([self.num_of_records + 1, self.popsize * self.num_independen_runs], dtype='f')
                    self.record_of_evolution_succeed = np.full([self.num_of_records + 1, self.popsize * self.num_independen_runs], False, dtype=bool)
                    self.graph_ticks = []

                    self.num_succeed = np.zeros(self.num_of_records + 1, dtype='i')

                for j in range(0, self.num_of_records + 1):
                    generation = j * (self.num_generations // self.num_of_records )
                    generation = 1 if (generation == 0) else generation
                    dset = f["gen_%s" % generation]
                    self.num_succeed[j] += dset.attrs['suceeded']
                    self.record_of_evolution[j, (i*self.popsize):((i*self.popsize)+self.popsize)] = dset

                    # pokud existuje informace o tom kolik uspelo, vyuzijeme ji, jinak si ji vygenerujeme sami dle poctu fitness vetsich jak 0
                    if ("/gen_succeed_%s" % generation) in f:
                        dset_succeed = f["gen_succeed_%s" % generation]
                        self.record_of_evolution_succeed[j, (i * self.popsize):((i * self.popsize) + self.popsize)] = dset_succeed
                    else:
                        tmp_mask = np.full(self.popsize, False, dtype=bool)
                        for k in range(0,dset.shape[0]):
                            if (dset[k] > 0):
                                tmp_mask[k] = True

                        f.create_dataset("gen_succeed_%s" % generation, data=tmp_mask)
                        self.record_of_evolution_succeed[j, (i * self.popsize):((i * self.popsize) + self.popsize)] = tmp_mask

        for j in range(0, self.num_of_records + 1):
            generation = j * (self.num_generations // self.num_of_records)
            generation = 1 if (generation == 0) else generation

            tick_val = ((generation * self.num_evaluations_per_generation) / 10000)
            if tick_val < 1.0:
                tick_val_str = "%g" % tick_val
            else:
                tick_val_str = "%.2f" % tick_val
                tick_val = float(tick_val_str)
                tick_val_str = "%g" % tick_val

            self.graph_ticks.append(tick_val_str)

            #self.graph_ticks.append("%g" % ((generation * self.num_evaluations_per_generation) / 10000)) # self.graph_ticks.append("%g\n(%g)" % ((generation * self.num_evaluations_per_generation) / 10000, generation / 100))

        self.record_of_evolution = list(self.record_of_evolution)
        self.record_of_evolution_succeed = list(self.record_of_evolution_succeed)

        """for idx, val in enumerate(self.record_of_evolution):
            num_all_succ = (self.record_of_evolution[idx] > 0).sum()
            #print(num_all_succ >= self.num_succeed[idx])
            print("%s | %s" % (num_all_succ, self.num_succeed[idx]))"""
            
        """for idx, val in enumerate(self.record_of_evolution):
            for idx2, val2 in enumerate(val):
                #conter += 1
                #if (not self.record_of_evolution_succeed[idx][idx2] and self.record_of_evolution[idx][idx2] > 0):
                #    print(self.record_of_evolution[idx][idx2])
                
                
                #if self.record_of_evolution_succeed[idx][idx2]:
                #    conter2 += 1
                if (self.record_of_evolution_succeed[idx][idx2] and self.record_of_evolution[idx][idx2] < 0):
                    print(self.record_of_evolution[idx][idx2])"""


    def plot_record(self, plot_path=None, algorithms_statistics=None, settings=None, num_col=2):

        rcParams['axes.titlepad'] = 25

        algorithms_statistics = algorithms_statistics if (algorithms_statistics is not None) else [self]
        settings = settings if (settings is not None) else {"description" : [os.path.basename(self.path)]}

        num_images = len(algorithms_statistics)
        ncols = 1 if (num_images == 1) else num_col
        nrows = 1 if (num_images == 1) else math.ceil(num_images / ncols)
        fig_width = 3.5 * ncols if (num_images > 1) else 4
        fig_height = (3.5 * nrows) + nrows * 0.2
        if (nrows == 2):
            fig_height += 0.1
        if (nrows == 1):
            fig_height += 0.4


        fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(fig_width, fig_height))

        medianprops = dict(color='mediumspringgreen', linewidth=1.5)
        flierprops = dict(markeredgecolor='lightsteelblue',linestyle='none')
        boxprops = dict(facecolor='steelblue', color="steelblue")
        whiskerprops = dict(color="steelblue", zorder=10, linewidth=1.5)
        capprops = dict(color="steelblue", zorder=10, linewidth=1.5)  # cornflowerblue

        # plot violin plot
        #axes[0].violinplot(self.record_of_evolution, showmeans=False, showmedians=True)
        #axes[0].set_title('Violin plot')

        # adding horizontal grid lines
        graph_num = 0
        for coords, ax in np.ndenumerate(axes):

            # empty graphs for which are not statistocs skip
            if len(algorithms_statistics) == graph_num:
                break

            stat_obj = algorithms_statistics[graph_num]

            ax.boxplot(stat_obj.record_of_evolution, patch_artist=True, capprops=capprops.copy(),
                               whiskerprops=whiskerprops.copy(), boxprops=boxprops.copy(),
                               flierprops=flierprops.copy(), medianprops=medianprops.copy())
            ax.set_title('%s: %s' % (stat_obj.evolution_algorithm, settings["description"][graph_num]))
            #ax.set_yscale('symlog', linthreshx=[-1,1])

            ax.yaxis.grid(True, which='major', color='lightgrey', alpha=0.8)
            ax.yaxis.grid(True, which='minor', color='lightgrey', alpha=0.4)
            ax.tick_params(axis='both', which='major', labelsize='small')
            ax.tick_params(axis='x', which='major', labelsize='x-small')
            ax.set_ylim(-26, 4)

            ax.set_xticks([y for y in range(1, stat_obj.num_of_records + 2)])
            ax.set_yticks(np.arange(-26, 5, 1), minor=True)
            ax.set_yticks(np.arange(-26, 5, 3))

            #for tick in ax.get_xticklabels():
            #    tick.set_rotation(30)

            exclude_xlabel = exclude_ylabel = False
            if (len(coords) > 0):
                exclude_xlabel = (coords[0] != nrows - 1) if (len(coords) > 1) else False
                exclude_ylabel = (coords[1] % ncols != 0) if (len(coords) > 1) else (coords[0] % ncols != 0)
            if (not exclude_xlabel):
                ax.set_xlabel('Evaluations [E+10000]') #ax.set_xlabel('Evaluations[E+10000] \n (Generation[E+100])')
            if (not exclude_ylabel):
                ax.set_ylabel('Fitness')

            pos = np.arange(len(stat_obj.record_of_evolution)) + 1
            for tick, label in zip(range(len(stat_obj.record_of_evolution)), ax.get_xticklabels()):
                # evolution run suceeed
                ax.text(pos[tick], 6 - (5 * 0.3), "%2.f" % ((stat_obj.num_succeed[tick] / stat_obj.num_independen_runs) * 100),
                        horizontalalignment='center', size='x-small', weight="normal",
                        color="steelblue")
                # percent of population succeed
                ax.text(pos[tick], 7.5 - (6 * 0.3), "%2.1f" % ((stat_obj.record_of_evolution_succeed[tick].sum() / stat_obj.record_of_evolution_succeed[tick].size) * 100),
                    horizontalalignment='center', size='x-small', weight="normal",
                    color="lightsteelblue")

            # add x-tick labels
            ax.set_xticklabels(stat_obj.graph_ticks)

            graph_num += 1

        # add x-tick labels
        #plt.setp(axes, xticks=[y for y in range(1, stat_obj.num_of_records + 2)],
        #         xticklabels=stat_obj.graph_ticks)

        fig.tight_layout(w_pad=1.3, h_pad=0.3)

        #plt.show()
        #fig.savefig('path/to/save/image/to.png')
        if plot_path == None:
            folder_name = os.path.basename(os.path.normpath(self.path))
            plot_path = self.path + '/../'+ folder_name +'_stats.pdf'

        plt.savefig(plot_path, bbox_inches='tight')

    def _detect_num_of_independent_runs(self):
        """
        Count hdf5 files (subrecords of evolution, subrecord is record of evolution for one independent run) and so detect number of independent runs of evolution
        :return:
        """
        num = 0
        while True:
            file = Path("%s/rec%s.hdf5" % (self.path, num))
            if file.exists():
                num += 1
            else:
                break

        return num


if __name__ == '__main__':
    #from xvfbwrapper import Xvfb
    #xvfb = Xvfb(width=1008, height=1440)
    #xvfb.start()

    prefix = "/scratch/work/user/xherec00/rocket/outputs/experiment2"
    algorithms = ["GA", "ES", "DE"]
    algorithms_GA = [
        "%s/GA/evolution_run_number_1" % prefix,
        "%s/GA/evolution_run_number_2" % prefix,
        "%s/GA/evolution_run_number_3" % prefix,
        "%s/GA/evolution_run_number_4" % prefix,
        "%s/GA/evolution_run_number_5" % prefix,
        "%s/GA/evolution_run_number_6" % prefix]

    algorithms_ES = [
        "%s/ES/evolution_run_number_1" % prefix,
        "%s/ES/evolution_run_number_2" % prefix,
        "%s/ES/evolution_run_number_3" % prefix,
        "%s/ES/evolution_run_number_4" % prefix,
        "%s/ES/evolution_run_number_5" % prefix,
        "%s/ES/evolution_run_number_6" % prefix]

    algorithms_DE = [
        "%s/DE/evolution_run_number_1" % prefix,
        "%s/DE/evolution_run_number_2" % prefix,
        "%s/DE/evolution_run_number_3" % prefix,
        "%s/DE/evolution_run_number_4" % prefix,
        "%s/DE/evolution_run_number_5" % prefix,
        "%s/DE/evolution_run_number_6" % prefix]

    skip_indexes = []  # which algorithm skip to plot

    for index, stats_paths in enumerate([algorithms_GA, algorithms_ES, algorithms_DE]):
        if index in skip_indexes:
            continue

        algorithms_statistics = []
        descriptions = []
        stats_obj = EvolutionStatistics("")
        plot_path = "summary_stats-%s.pdf" % algorithms[index]

        for index2, stat_path in enumerate(stats_paths):
            stats = EvolutionStatistics(stat_path)
            stats.load_record_of_evolution()
            algorithms_statistics.append(stats)
            descriptions.append("var%s" % str(index2 + 1))
            # stats_obj.plot_record()

        stats_obj.plot_record(plot_path=plot_path, algorithms_statistics=algorithms_statistics,
                              settings={"description": descriptions}, num_col=2)

    """for i in [1,2,3,4,5,6,7,8]:
        stats = EvolutionStatistics("evolution_run_number_%s" % i)
        stats.load_record_of_evolution()
        stats.plot_record()"""

    #xvfb.stop()