import argparse
import sys
import numpy as np

from scipy.optimize import differential_evolution

import gym
from gym import wrappers, logger

from keras.models import Sequential
from keras.layers import Dense
from keras import backend as K

from keras.utils import plot_model
from keras.layers.advanced_activations import LeakyReLU

from deap import base
from deap import creator
from deap import tools

import random
import time

from mpi4py import MPI
import h5py

import os
import shutil

import math

import EvolutionStatistics
from globb import Global

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()
status = MPI.Status()   # get MPI status object


class MPILogFile(object):
    def __init__(self, comm, filename, mode):
        self.file_handle = MPI.File.Open(comm, filename, mode)
        self.file_handle.Set_atomicity(True)
        self.buffer = bytearray

    def write(self, msg):
        b = bytearray()
        b.extend(map(ord, msg))
        self.file_handle.Write_shared(b)

    def close(self):
        self.file_handle.Sync()
        self.file_handle.Close()

logfile = MPILogFile(
    comm, str(os.environ['NEUROEVOLUTION_OUTPUT_DIR']) + "/out-fitneses.txt",
    MPI.MODE_WRONLY | MPI.MODE_CREATE | MPI.MODE_APPEND
)

logfile2 = MPILogFile(
    comm, str(os.environ['NEUROEVOLUTION_OUTPUT_DIR']) + "/out-allfitneses.txt",
    MPI.MODE_WRONLY | MPI.MODE_CREATE | MPI.MODE_APPEND
)


file2 = open(str(os.environ['NEUROEVOLUTION_OUTPUT_DIR']) + "/out-" + str(rank) + ".txt", "w")

class EvolutionProcess(object):
    GA = 1
    ES = 2
    DE = 3

    def __init__(self, simulator, agent):
        self.simulator = simulator
        self.agent = agent
        self.evolution_statistics = EvolutionStatistics.EvolutionStatistics(Global.CONFIG['OUTPUT_DIR_NAME'])

        creator.create("FitnessMax", base.Fitness, weights=(1.0,))
        self.toolbox = base.Toolbox()

        self._create_record_of_evolution()

    def save_plot_evolution_statistics(self):
        self.evolution_statistics.load_record_of_evolution()
        self.evolution_statistics.plot_record()

    def fitnessPopulationEvaluate(self, pop):

        for i in range(len(pop)):
            self.agent.importWeightsToModel(pop[i])
            fitness, additional_info = self.simulator.simulation(Global.CONFIG['NUM_SIMULATION_PER_FITNESS'])
            pop[i].fitness.values = (fitness, ) # must be tuple because of multiobjective optimalization
            pop[i].succeed = additional_info['landed_succesfully'] #info about success of individual

    def _create_record_of_evolution(self):
        if Global.EA_CONFIG['USED_EA'] == EvolutionProcess.GA:
            popsize = Global.EA_CONFIG['POPULATION_SIZE']
            num_evaluations_per_generation = Global.EA_CONFIG['POPULATION_SIZE']
            evolution_algorithm = "GA"
        elif Global.EA_CONFIG['USED_EA'] == EvolutionProcess.ES:
            popsize = Global.EA_CONFIG['POPULATION_SIZE']
            num_evaluations_per_generation = Global.EA_CONFIG['ES_NUM_CHILDREN']
            evolution_algorithm = "ES"
        elif Global.EA_CONFIG['USED_EA'] == EvolutionProcess.DE:
            popsize = Global.EA_CONFIG['POPULATION_SIZE']
            num_evaluations_per_generation = Global.EA_CONFIG['POPULATION_SIZE']
            evolution_algorithm = "DE"

        self.evolution_statistics.create_record_of_evolution(rank=rank, num_generations=Global.EA_CONFIG['NGEN'], popsize=popsize, num_independen_runs=nprocs, num_evaluations_per_generation=num_evaluations_per_generation, num_of_records=Global.CONFIG['NUM_RECORDS'], evolution_algorithm = evolution_algorithm, evolution_algorithm_settings=Global.EA_CONFIG, other_settings=Global.CONFIG)

    def _evolution_start(self):

        file2.write("%s\n" % str(Global.EA_CONFIG))
        file2.write("%s\n" % str(Global.CONFIG))

        if rank == 0:
            print("Start of evolution with random seed :%s" % Global.CONFIG['SEED'])
        file2.write("Start of evolution with random seed :%s\n" % Global.CONFIG['SEED'])

        self.start_time = MPI.Wtime()

        pop = self.toolbox.population(n=Global.EA_CONFIG['POPULATION_SIZE'])

        # Evaluate the entire population
        self.fitnessPopulationEvaluate(pop)
        #print("  Evaluated %i individuals" % len(pop))
        #file2.write("Evaluated %i individuals\n" % len(pop))
        file2.flush()
        os.fsync(file2.fileno())

        return pop

    def _generation_start(self, g):

        self.start_start_time = MPI.Wtime()
        if rank == 0:
            print("-- Generation %i --" % g)
        file2.write("-- Generation %i --\n" % g)

    def _generation_finished(self, pop, g):
        save_progress = g % (Global.EA_CONFIG['NGEN'] // Global.CONFIG['NUM_SAVED_PROGRESS']) == 0 or g == 1 # flag if save progress
        save_evolution_record = g % (Global.EA_CONFIG['NGEN'] // Global.CONFIG['NUM_RECORDS']) == 0 or g == 1 # flag if save evolution record
        save_video = g % (Global.EA_CONFIG['NGEN'] // Global.CONFIG['NUM_VIDEOS']) == 0
        # if not special flag we should save video from 1. generation
        if Global.CONFIG['RENDER_VIDEO_FOR_FIRST_GENERATION']:
            save_video = save_video or g == 1

        if save_progress or save_evolution_record or save_video:

            # Gather all the fitnesses in one list and print the stats
            fits = [ind.fitness.values[0] for ind in pop]
            succeed_individuals = [ind.succeed for ind in pop]
            sum_succ = sum(succeed_individuals)

            length = len(pop)
            mean = sum(fits) / length
            sum2 = sum(x * x for x in fits)
            std = abs(sum2 / length - mean ** 2) ** 0.5
            min_fit = min(fits)
            max_fit = max(fits)

            if save_progress:
                file2.write("G: %s | Min: %s | Max: %s | Avg: %s | Std: %s | Succ: %s| Rank: %s\n" % (g, min_fit, max_fit, mean, std, sum_succ,rank))
                logfile2.write("G: %s | Min: %s | Max: %s | Avg: %s | Std: %s | Succ: %s| Rank: %s\n" % (g, min_fit, max_fit, mean, std, sum_succ,rank))
                end = MPI.Wtime()
                file2.write("time elapsed in round/total: %s/%s\n" % (str(end - self.start_start_time), str(end - self.start_time)))

            if save_evolution_record:
                self.evolution_statistics.append_data_to_record(fitness_values=fits,
                                                                succeeded=any(ind.succeed == True for ind in pop),
                                                                generation=g,
                                                                succeed_individuals=succeed_individuals)


                logfile.write("G: %s | Min: %s | Max: %s | Avg: %s | Std: %s | Succ: %s| Rank: %s\n" % (g, min_fit, max_fit, mean, std, sum_succ,rank))

            # 10x behem evoluce vytvorime video z chovani agenta a vypiseme jeho genom
            if save_video:
                best_ind = tools.selBest(pop, 1)[0]
                worst_ind = tools.selWorst(pop, 1)[0]

                isbestsucceed = "BEST_IS_SUCC" if best_ind.succeed == True else "BEST_NOT_SUCC"
                if best_ind.succeed != True:
                    for ind in pop:
                        if ind.succeed:
                            best_ind = ind
                            break

                if g == Global.EA_CONFIG['NGEN']:
                    file2.write("G: %s, Rank: %s, Succ: %s, Best individual is:\n %s, %s | %s\n" % (g, rank, sum_succ, best_ind, best_ind.fitness.values, isbestsucceed))
                else:
                    file2.write("G: %s, Rank: %s, Succ: %s, Best individual fitness is: %s | %s\n" % (g, rank, sum_succ, best_ind.fitness.values, isbestsucceed))
                file2.flush()
                os.fsync(file2.fileno())

                self.agent.importWeightsToModel(best_ind)
                self.simulator.simulation(record=True)

                # for video we save agent(rocket controller) weights
                path_base, actual_ext = os.path.splitext(self.simulator.env.video_recorder.path)
                path_to_model_weights = '%s/%s.h5' % (Global.CONFIG['OUTPUT_INDIVIDUALS_IN_VIDEO_DIR_NAME'], os.path.basename(path_base))
                self.agent.model.save_weights(path_to_model_weights)

                # self.agent.importWeightsToModel(worst_ind)
                # self.simulator.simulation(episodes=1, record=True)


    def _evolution_finished(self):
        if rank == 0:
            print("-- End of (successful) evolution --")

        file2.write("-- End of (successful) evolution --\n")

        end = MPI.Wtime()
        file2.write("time elapsed: " + str(end - self.start_time) + "\n")
        file2.flush()
        os.fsync(file2.fileno())

        logfile.close()
        logfile2.close()
        file2.close()


class EvolutionProcessGA(EvolutionProcess):

    CONFIG = {}

    def __init__(self, simulator, agent):
        super().__init__(simulator, agent)

        creator.create("Individual", list, fitness=creator.FitnessMax, succeed=False)

        self.toolbox.register("individual", self.individualInit, container=creator.Individual)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)

        # Operator registering
        if Global.EA_CONFIG['CROSS_UNIFORM']:
            self.toolbox.register("mate", tools.cxUniform, indpb=Global.EA_CONFIG['INDPB_UNIFORM_CROSSOVER'])
        else:
            self.toolbox.register("mate", tools.cxTwoPoint)

        self.toolbox.register("mutate", self.mutation, indpb=Global.EA_CONFIG['INDPB'])
        self.toolbox.register("select", tools.selTournament, tournsize=Global.EA_CONFIG['TOURNAMENT_SIZE'])

    def individualInit(self, container):
        individualGenome = [random.uniform(Global.EA_CONFIG['MIN_WEIGHT'], Global.EA_CONFIG['MAX_WEIGHT']) for _ in range(self.agent.weight_total_number)] #původn random.random

        return container(individualGenome)

    def mutation(self, individualGenome, indpb):

        # orizneme vysledek po pricteni gaussovkskho prirusteku na interval 0-1
        for gene in range(self.agent.weight_total_number):
            if random.random() < indpb:
                if (Global.EA_CONFIG['GAUSSIAN_MUTATION']):
                    individualGenome[gene] = max(Global.EA_CONFIG['MIN_WEIGHT'], min(individualGenome[gene] + random.gauss(0.0, Global.EA_CONFIG['SIGMA']), Global.EA_CONFIG['MAX_WEIGHT'])) #max(0.0, min(individualGenome[gene] + random.gauss(0.0, Global.EA_CONFIG['SIGMA']), 1.0))
                else:
                    individualGenome[gene] = random.uniform(Global.EA_CONFIG['MIN_WEIGHT'], Global.EA_CONFIG['MAX_WEIGHT']) #random.random()


    def evolve(self):

        pop = self._evolution_start()

        # Begin the evolution
        for g in range(1, Global.EA_CONFIG['NGEN'] + 1):

            self._generation_start(g)

            # Select the next generation individuals
            offspring = self.toolbox.select(pop, len(pop) - 1)  # len(pop) - 1 = elitism | len(pop) = not elitism
            # Clone the selected individuals
            offspring = [self.toolbox.clone(tools.selBest(pop, 1)[0])] + list(map(self.toolbox.clone, offspring))
            #           [..............elitism..................]

            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[1::2], offspring[2::2]):  # [1::2] and [2::2] = elitism, first is elite | [::2] and [1::2] = not elitism
                if Global.EA_CONFIG['CROSS_ENABLED']:
                    if random.random() < Global.EA_CONFIG['CXPB']:
                        self.toolbox.mate(child1, child2)
                        del child1.fitness.values
                        del child2.fitness.values

            # apply mutation, elitism = offspring[1:] | not elitism = offspring
            for mutant in offspring[1:]:
                if random.random() < Global.EA_CONFIG['MUTPB']:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values

            # Evaluate the individuals with an invalid fitness
            #invalid_indexes_of_individual = [i for i in range(len(offspring)) if not offspring[i].fitness.valid]
            self.fitnessPopulationEvaluate(offspring)

            #print("  Evaluated %i individuals" % len(invalid_indexes_of_individual))
            #file2.write("Evaluated %i individuals\n" % len(invalid_indexes_of_individual))

            # The population is entirely replaced by the offspring
            pop[:] = offspring

            self._generation_finished(pop, g)

        self._evolution_finished()


class EvolutionProcessES(EvolutionProcess):

    CONFIG = {}

    def __init__(self, simulator, agent):
        super().__init__(simulator, agent)

        self.tau = 1 / math.sqrt(self.agent.weight_total_number) # arametr tau pouzivany pri mutaci

        creator.create("Individual", list, fitness=creator.FitnessMax, strategy=None, succeed=False)

        self.toolbox.register("individual", self.individualInit, container=creator.Individual)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)

        # Operator registering
        self.toolbox.register("mate", self.intermediateCrossover)
        self.toolbox.register("mutate", self.mutation)
        self.toolbox.register("select", tools.selRandom)

    def individualInit(self, container):
        individualGenome = [random.uniform(Global.EA_CONFIG['MIN_WEIGHT'], Global.EA_CONFIG['MAX_WEIGHT']) for _ in range(self.agent.weight_total_number)] #původn random.random
        new_ind = container(individualGenome)
        new_ind.strategy = random.uniform(Global.EA_CONFIG['MIN_STRATEGY'], Global.EA_CONFIG['MAX_STRATEGY'])
        return new_ind

    def uniformCross(self, ind1, ind2):
        # cross solution vector
        for gene in range(len(ind1)):
            if random.random() < 0.5:
                tmp_gene = ind1[gene]
                ind1[gene] = ind2[gene]
                ind2[gene] = tmp_gene

        # cross strategy vector
        if random.random() < 0.5:
            tmp_strategy = ind1.strategy
            ind1.strategy = ind2.strategy
            ind2.strategy = tmp_strategy

    def intermediateCrossover(self, ind1, ind2):
        # cross solution vector
        r = random.random()
        for gene in range(len(ind1)):
            ind1[gene] = r * ind1[gene] + (1. - r) * ind2[gene]
            ind2[gene] = r * ind2[gene] + (1. - r) * ind1[gene]

        # cross strategy vector
        r = random.random()
        ind1.strategy = r * ind1.strategy + (1. - r) * ind2.strategy
        ind2.strategy = r * ind2.strategy + (1. - r) * ind1.strategy

        return ind1, ind2

    def mutation(self, individualGenome):

        individualGenome.strategy = individualGenome.strategy * math.exp(self.tau * random.gauss(0, 1))  # mutate startegy vcetor

        for gene in range(self.agent.weight_total_number):
            mutation = individualGenome.strategy * random.gauss(0, 1)
            mutation_result = individualGenome[gene] + mutation # zjistime vysledek mutace

            mutation_result_sign = mutation_result // abs(mutation_result) # zjistime znamenko vysledku mutace
            # pokud mutace hranici prekrocila pristoupime ke korekci kdy se budeme blizit smerme k hranici
            if (abs(mutation_result) > Global.EA_CONFIG['MAX_WEIGHT']):
                distance_to_bound = (Global.EA_CONFIG['MAX_WEIGHT'] * mutation_result_sign) - individualGenome[gene] # vzdalenost aktualni hodnoty genu k hranicni hodnote genu, ktera byla mutaci prekrocena
                individualGenome[gene] = individualGenome[gene] + distance_to_bound * 0.8 # mutate solution vectro,  posuneme se směrem k hranici o 0.8 nasobek vzdalenosti k ni (pokud mutace tuto hranici překrocila)
            # pokud mutace hranici neprekrocila, tak je vse v poradku a korekce nedelame a aplikujeme ji
            else:
                individualGenome[gene] = mutation_result # mutate solution vectro


    def evolve(self):

        pop = self._evolution_start()

        # Begin the evolution
        for g in range(1, Global.EA_CONFIG['NGEN'] + 1):

            self._generation_start(g)

            # Select the next generation individuals
            offspring = self.toolbox.select(pop, Global.EA_CONFIG['ES_NUM_CHILDREN'])  # len(pop) - 1 = elitism | len(pop) = not elitism
            # Clone the selected individuals
            offspring = list(map(self.toolbox.clone, offspring))

            # Apply crossover and mutation on the offspring
            if Global.EA_CONFIG['CROSS_ES_ENABLED']:
                for child1, child2 in zip(offspring[::2], offspring[1::2]):  # [1::2] and [2::2] = elitism, first is elite | [::2] and [1::2] = not elitism
                    if random.random() < Global.EA_CONFIG['ES_CXPB']:
                        self.toolbox.mate(child1, child2)

            # apply mutation only on liche offspringy protoze jenom v nich je provedeno aritmeticke krizeni
            for mutant in offspring: #for mutant in offspring[0::2]:
                self.toolbox.mutate(mutant)

            # Evaluate the individuals
            self.fitnessPopulationEvaluate(offspring) # self.fitnessPopulationEvaluate(offspring[0::2], None)

            # The population is replaced by best of parents a and childs
            if (Global.EA_CONFIG['ES_COMMA_STRATEGY']):
                # comma startegy + elitism
                pop[:] = tools.selBest(offspring + [self.toolbox.clone(tools.selBest(pop, 1)[0])], Global.EA_CONFIG['POPULATION_SIZE']) #pop[:] = tools.selBest(offspring[0::2] + list(map(self.toolbox.clone, pop)), Global.EA_CONFIG['POPULATION_SIZE'])
            # plus startegy
            else:
                pop[:] = tools.selBest(offspring + list(map(self.toolbox.clone, pop)), Global.EA_CONFIG['POPULATION_SIZE']) #pop[:] = tools.selBest(offspring[0::2] + list(map(self.toolbox.clone, pop)), Global.EA_CONFIG['POPULATION_SIZE'])

            self._generation_finished(pop, g)

        self._evolution_finished()


class EvolutionProcessDENew(EvolutionProcess):

    CONFIG = {}

    def __init__(self, simulator, agent):
        super().__init__(simulator, agent)

        creator.create("Individual", list, fitness=creator.FitnessMax, strategy=None, succeed=False)

        self.toolbox.register("individual", self.individualInit, container=creator.Individual)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)

        # Operator registering
        self.toolbox.register("select", self.selectRandomUniqueExceptCurrentIndividual)
        self.toolbox.register("evaluate", self.fitnessEvaluateIndividual)

    def selectRandomUniqueExceptCurrentIndividual(self, pop, current_individual_index):

        candidate_parents = list(range(len(pop)))
        candidate_parents.remove(current_individual_index)  # nebudmee vybirat za rodice element ktery prave mutujeme

        if Global.EA_CONFIG['ALGORITHM_DE'] == "rand1bin":
            random_parents_indexes = random.sample(candidate_parents, 3)

            return [pop[i] for i in random_parents_indexes]

        elif Global.EA_CONFIG['ALGORITHM_DE'] == "best2bin":
            random_parents_indexes = random.sample(candidate_parents, 4)

            return [tools.selBest(pop, 1)[0]] + [pop[i] for i in random_parents_indexes]

    def individualInit(self, container):
        individualGenome = [random.uniform(Global.EA_CONFIG['MIN_WEIGHT'], Global.EA_CONFIG['MAX_WEIGHT']) for _ in range(self.agent.weight_total_number)] #původn random.random
        return container(individualGenome)

    def fitnessEvaluateIndividual(self, ind):

        self.agent.importWeightsToModel(ind)
        fitness, additional_info = self.simulator.simulation(Global.CONFIG['NUM_SIMULATION_PER_FITNESS'])
        ind.fitness.values = (fitness,)  # must be tuple because of multiobjective optimalization
        ind.succeed = additional_info['landed_succesfully']  # info about success of individual

        #return (self.simulator.simulation(Global.CONFIG['NUM_SIMULATION_PER_FITNESS']), ) # must be tuple because of multiobjective optimalization

    def checkBounds(self, gene, mutation):

        mutation_result = gene + mutation  # zjistime vysledek mutace

        if Global.EA_CONFIG['MUTATION_SLICE_BOUNDS']:
            if (mutation_result > Global.EA_CONFIG['MAX_WEIGHT']):
                return Global.EA_CONFIG['MAX_WEIGHT']
            elif(mutation_result < Global.EA_CONFIG['MIN_WEIGHT']):
                return Global.EA_CONFIG['MIN_WEIGHT']
            else:
                return mutation_result
        else:
            mutation_result_sign = mutation_result // abs(mutation_result)  # zjistime znamenko vysledku mutace
            # pokud mutace hranici prekrocila pristoupime ke korekci kdy se budeme blizit smerme k hranici
            if (abs(mutation_result) > Global.EA_CONFIG['MAX_WEIGHT']):
                distance_to_bound = (Global.EA_CONFIG['MAX_WEIGHT'] * mutation_result_sign) - gene # vzdalenost aktualni hodnoty genu k hranicni hodnote genu, ktera byla mutaci prekrocena
                return gene + distance_to_bound * 0.8  # mutate solution vectro,  posuneme se směrem k hranici o 0.8 nasobek vzdalenosti k ni (pokud mutace tuto hranici překrocila)
            # pokud mutace hranici neprekrocila, tak je vse v poradku a korekce nedelame a aplikujeme ji
            else:
                return mutation_result  # mutate solution vectro

    def evolve(self):

        pop = self._evolution_start()

        # Begin the evolution
        for g in range(1, Global.EA_CONFIG['NGEN'] + 1):

            self._generation_start(g)

            for current_individual_index, current_individual in enumerate(pop):
                new_individual = self.toolbox.clone(current_individual) # hned si naklonujeme jedince
                rand_gene_index = random.randint(0, len(new_individual))

                if Global.EA_CONFIG['ALGORITHM_DE'] == "rand1bin":
                    rand_p1, p2, p3 = self.toolbox.select(pop, current_individual_index=current_individual_index)

                    for gene_index in range(len(new_individual)):
                        if rand_gene_index == gene_index or random.random() < Global.EA_CONFIG['DE_CR']:
                            new_individual[gene_index] = self.checkBounds(rand_p1[gene_index], Global.EA_CONFIG['DE_F'] * (p2[gene_index] - p3[gene_index]))

                elif Global.EA_CONFIG['ALGORITHM_DE'] == "best2bin":
                    best_p1, p2, p3, p4, p5 = self.toolbox.select(pop, current_individual_index=current_individual_index)

                    for gene_index in range(len(new_individual)):
                        if rand_gene_index == gene_index or random.random() < Global.EA_CONFIG['DE_CR']:
                            new_individual[gene_index] = self.checkBounds(best_p1[gene_index], Global.EA_CONFIG['DE_F'] * (p2[gene_index] - p3[gene_index]) + Global.EA_CONFIG['DE_F'] * (p4[gene_index] - p5[gene_index]))


                self.toolbox.evaluate(new_individual) #new_individual.fitness.values = self.toolbox.evaluate(new_individual)

                if new_individual.fitness >= current_individual.fitness:
                    pop[current_individual_index] = new_individual

            self._generation_finished(pop, g)

        self._evolution_finished()


class EvolutionProcessDE(object):

    def __init__(self, simulator, agent):
        self.simulator = simulator
        self.agent = agent
        self.generation = 0
        self.min_fit = 100
        self.evaluated_ind = 0

    def fitnessPopulationEvaluate(self, ind):
        if (self.evaluated_ind % Global.EA_CONFIG['POPULATION_SIZE'] == 0):
            self.min_fit = 100
        self.agent.importWeightsToModel(ind)
        fitness = self.simulator.simulation(Global.CONFIG['NUM_SIMULATION_PER_FITNESS'])
        if (-fitness < self.min_fit):
            self.min_fit = fitness

        self.evaluated_ind += 1
        return -fitness


    def callbackEvolutionProgress(self, best_ind, convergence):

        g = self.generation

        print("-- Generation %i --" % g)
        file2.write("-- Generation %i --\n" % g)

        file2.write("Max: %s\n" % (self.min_fit))
        logfile2.write("G: %s | Max: %s | Rank: %s\n" % (g, self.min_fit, rank))

        # 10x behem evoluce vytvorime video z chovani agenta a vypiseme jeho genom
        if (g % (Global.EA_CONFIG['NGEN'] // NUM_VIDEOS) == 0):
            print("G: %s, Rank: %s, Best individual is:\n %s, %s\n" % (g, rank, best_ind, self.min_fit))
            file2.write("G: %s, Rank: %s, Best individual is:\n %s, %s\n" % (g, rank, best_ind, self.min_fit))
            file2.flush()
            os.fsync(file2.fileno())
            self.agent.importWeightsToModel(best_ind)
            self.simulator.simulation(episodes=1, record=True)
            #self.agent.importWeightsToModel(worst_ind)
            #self.simulator.simulation(episodes=1, record=True)

            # vypiseme ranky nejlepsich
            #comm.Barrier()
            logfile.write("G: %s | Max: %s | Rank: %s\n" % (g, self.min_fit, rank))
            print("G: %s, Rank: %s, best is: %s" % (g, rank, best_ind, self.min_fit))

        self.generation += 1

    def evolve(self):
        start = MPI.Wtime()

        # Begin the evolution

        bounds = [(Global.EA_CONFIG['MIN_WEIGHT'], Global.EA_CONFIG['MAX_WEIGHT']) for _ in range(self.agent.weight_total_number)]

        result = differential_evolution(self.fitnessPopulationEvaluate, bounds, args=(), strategy=Global.EA_CONFIG['ALGORITHM_DE'], maxiter=Global.EA_CONFIG['NGEN'], popsize=Global.EA_CONFIG['POPULATION_SIZE'], tol=0.01,
                               mutation=(0.5, 1), recombination=0.7, seed=Global.CONFIG['SEED'], callback=self.callbackEvolutionProgress, disp=False, polish=False,
                               init='latinhypercube')

        #result.x, result.fun

        print("-- End of (successful) evolution --")

        end = MPI.Wtime()
        file2.write("time elapsed: " + str(end - start) + "\n")
        file2.flush()
        os.fsync(file2.fileno())



