#!/bin/bash
#module load Bash/4.4-intel-2017b 
#module load zsh/5.2-foss-2018a
output_dir="/scratch/work/user/xherec00/rocket/outputs/tests/test-freefall-mass-init-conditions"
rm -rf ${output_dir}/

mkdir -p ${output_dir}
cp /home/xherec00/gym-openai/rocket/neuroevolution.py /home/xherec00/gym-openai/rocket/evolution_algorithms.py /home/xherec00/gym-openai/rocket/EvolutionStatistics.py /home/xherec00/gym-openai/rocket/globb.py /home/xherec00/gym/gym/envs/box2d/rocket_lander.py ${output_dir}

#unset module

for i in `seq -w $1`
do
    qsub -v evolution_run_number=${i},output_dir=${output_dir} -N evolution_run_number_${i} run.pbs
    sleep 1
done
