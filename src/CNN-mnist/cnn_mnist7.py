
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from mpi4py import MPI
import numpy as np
import tensorflow as tf
from tensorflow.python.framework import dtypes
import time
import shutil
import sys

import random

from deap import base
from deap import creator
from deap import tools

import h5py
import os
import math

from evolution_algorithms import EvolutionProcess, EvolutionProcessGA, EvolutionProcessES
from globb import Global
import EvolutionStatistics


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()
status = MPI.Status()   # get MPI status object

random_seed = int(os.environ['SEED'])
random.seed(random_seed) # nastavime seed


JOB_NAME = str(os.environ['PBS_JOBID'])
run_number = str(os.environ['NEUROEVOLUTION_EVOLUTION_RUN_NUMBER']).lstrip("0")
INDEPENDENT_RUN = int(str(os.environ['INDEPENDENT_RUN']).lstrip("0"))
NUM_INDEPENDENT_RUNS = int(os.environ['NUM_INDEPENDENT_RUNS'])
OUTPUT_DIR_NAME = str(os.environ['NEUROEVOLUTION_OUTPUT_DIR'])
OUTPUT_MODEL_DIR_NAME = OUTPUT_DIR_NAME + "/../model" + str(INDEPENDENT_RUN)
OUTPUT_INDIVIDUALS_IN_MODEL_DIR_NAME = OUTPUT_MODEL_DIR_NAME + "/individuals"
os.makedirs(os.path.dirname(OUTPUT_MODEL_DIR_NAME), exist_ok=True)
os.makedirs(OUTPUT_INDIVIDUALS_IN_MODEL_DIR_NAME, exist_ok=True)

file2 = open(OUTPUT_DIR_NAME + "/out-" + str(rank) + ".txt", "w")
if rank == 0:
    file_common = open(OUTPUT_DIR_NAME + "/../out-succ.txt", "a")

MODEL_PATH = "/ramdisk/" + str(JOB_NAME) + "/mnist_convnet_model/" + str(rank)
MNIST_DATA_DIR = "/home/xherec00/tensorflow/cnn/MNIST-data/"

Global.CONFIG['SEED'] = random_seed
Global.CONFIG['JOB_NAME'] = JOB_NAME
Global.CONFIG['INDEPENDENT_RUN'] = INDEPENDENT_RUN
Global.CONFIG['NUM_INDEPENDENT_RUNS'] = NUM_INDEPENDENT_RUNS
Global.CONFIG['OUTPUT_DIR_NAME'] = OUTPUT_DIR_NAME
Global.CONFIG['OUTPUT_MODEL_DIR_NAME'] = OUTPUT_MODEL_DIR_NAME
Global.CONFIG['OUTPUT_INDIVIDUALS_IN_MODEL_DIR_NAME'] = OUTPUT_INDIVIDUALS_IN_MODEL_DIR_NAME
Global.CONFIG['NUM_MODELS'] = 5
Global.CONFIG['RENDER_MODEL_FOR_FIRST_GENERATION'] = True
Global.CONFIG['NUM_RECORDS'] = 5
Global.CONFIG['NUM_SAVED_PROGRESS'] = 5
Global.CONFIG['NUM_SIMULATION_PER_FITNESS'] = NUM_SIMULATION_PER_FITNESS = 1
Global.CONFIG['RUN_NUMBER'] = run_number
Global.CONFIG['LEARNING_RATE'] = 0.15

#NEURAL NET
Global.EA_CONFIG['USED_EA'] = EvolutionProcess.ES
Global.EA_CONFIG['NUM_EVALUATION'] = 1380#1840#920#2760#276
Global.EA_CONFIG['DEFAULT_POP_SIZE'] = int(str(os.environ['DEFAULT_POP_SIZE']).lstrip("0"))

EvolutionProcessGA.CONFIG['POPULATION_SIZE'] = Global.EA_CONFIG['DEFAULT_POP_SIZE']
EvolutionProcessGA.CONFIG['CXPB'] = 0.7
EvolutionProcessGA.CONFIG['MUTPB'] = 1.0
EvolutionProcessGA.CONFIG['CROSS_ENABLED'] = True
EvolutionProcessGA.CONFIG['CROSS_UNIFORM'] = True
EvolutionProcessGA.CONFIG['INDPB'] = 0.1
EvolutionProcessGA.CONFIG['INDPB_UNIFORM_CROSSOVER'] = 0.5
EvolutionProcessGA.CONFIG['TOURNAMENT_SIZE'] = 4

EvolutionProcessES.CONFIG['POPULATION_SIZE'] = Global.EA_CONFIG['DEFAULT_POP_SIZE']
EvolutionProcessES.CONFIG['ES_NUM_CHILDREN'] = EvolutionProcessES.CONFIG['POPULATION_SIZE']
EvolutionProcessES.CONFIG['CROSS_ES_ENABLED'] = False
EvolutionProcessES.CONFIG['INDPB'] = 0.1
EvolutionProcessES.CONFIG['ES_INDPB_UNIFORM_CROSSOVER'] = 0.5
EvolutionProcessES.CONFIG['ES_CXPB'] = 0.7
EvolutionProcessES.CONFIG['ES_COMMA_STRATEGY'] = False

# GA
"""if run_number == "1":
    pass
elif run_number == "2":
    EvolutionProcessGA.CONFIG['CROSS_ENABLED'] = False
elif run_number == "3":
    EvolutionProcessGA.CONFIG['INDPB'] = 0.05
    EvolutionProcessGA.CONFIG['CXPB'] = 0.9
    EvolutionProcessGA.CONFIG['TOURNAMENT_SIZE'] = 8"""

if run_number == "1":
    EvolutionProcessGA.CONFIG['POPULATION_SIZE'] = Global.EA_CONFIG['DEFAULT_POP_SIZE'] * 2
    EvolutionProcessGA.CONFIG['TOURNAMENT_SIZE'] = 8
elif run_number == "2":
    EvolutionProcessGA.CONFIG['POPULATION_SIZE'] = Global.EA_CONFIG['DEFAULT_POP_SIZE'] * 2
    EvolutionProcessGA.CONFIG['CROSS_ENABLED'] = False
elif run_number == "3":
    EvolutionProcessGA.CONFIG['POPULATION_SIZE'] = Global.EA_CONFIG['DEFAULT_POP_SIZE'] * 2

# ES
"""if run_number == "1":
    pass
elif run_number == "2":
    EvolutionProcessES.CONFIG['ES_COMMA_STRATEGY'] = True
    EvolutionProcessES.CONFIG['ES_NUM_CHILDREN'] = EvolutionProcessES.CONFIG['POPULATION_SIZE'] * 2
elif run_number == "3":
    EvolutionProcessES.CONFIG['CROSS_ES_ENABLED'] = True"""

if run_number == "1":
    EvolutionProcessES.CONFIG['POPULATION_SIZE'] = Global.EA_CONFIG['DEFAULT_POP_SIZE'] * 2
    EvolutionProcessES.CONFIG['CROSS_ES_ENABLED'] = True
    EvolutionProcessGA.CONFIG['INDPB'] = 0.05
    EvolutionProcessGA.CONFIG['ES_CXPB'] = 0.9
elif run_number == "2":
    EvolutionProcessES.CONFIG['POPULATION_SIZE'] = Global.EA_CONFIG['DEFAULT_POP_SIZE'] * 2
elif run_number == "3":
    EvolutionProcessES.CONFIG['POPULATION_SIZE'] = Global.EA_CONFIG['DEFAULT_POP_SIZE'] * 2
    EvolutionProcessES.CONFIG['ES_COMMA_STRATEGY'] = True
    EvolutionProcessES.CONFIG['ES_NUM_CHILDREN'] = EvolutionProcessES.CONFIG['POPULATION_SIZE'] * 2


if Global.EA_CONFIG['USED_EA'] == EvolutionProcess.GA:
    Global.EA_CONFIG.update(EvolutionProcessGA.CONFIG)
    Global.EA_CONFIG['NGEN'] = Global.EA_CONFIG['NUM_EVALUATION'] // Global.EA_CONFIG['POPULATION_SIZE']
elif Global.EA_CONFIG['USED_EA'] == EvolutionProcess.ES:
    Global.EA_CONFIG.update(EvolutionProcessES.CONFIG)
    Global.EA_CONFIG['NGEN'] = Global.EA_CONFIG['NUM_EVALUATION'] // Global.EA_CONFIG['ES_NUM_CHILDREN']

np.random.seed(random_seed)


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # nastavime logovani jen chyb, nebudeme vypisovat nic jineho, dalsi moznosti: 2 - warning, 1 - info, 0 - all
tf.logging.set_verbosity(tf.logging.ERROR)
#logger.set_level(logger.ERROR) # logger.set_level(logger.INFO)

Global.CNN_CONFIG['activation_functions'] = activation_functions = [tf.nn.relu, tf.nn.sigmoid, tf.nn.tanh] # Activation Functions: sigmoid, tanh, elu, softplus, and softsign, relu, relu6, crelu and relu_x, and random regularization (dropout).
Global.CNN_CONFIG['polling_types'] = polling_types = [tf.layers.average_pooling2d, tf.layers.max_pooling2d]

"""CXPB, MUTPB, NGEN = 0.7, 0.7, 1#30
INDPB = 0.2
POPULATION_SIZE = 23
TOURNAMENT_SIZE = 6"""

Global.CNN_CONFIG['MAX_FILTERS_NUM'] = MAX_FILTERS_NUM = 70
Global.CNN_CONFIG['MIN_FILTERS_NUM'] = MIN_FILTERS_NUM = 5

Global.CNN_CONFIG['MAX_FILTER_SIZE'] = MAX_FILTER_SIZE = 10
Global.CNN_CONFIG['MIN_FILTER_SIZE'] = MIN_FILTER_SIZE = 2

Global.CNN_CONFIG['MAX_ACTIVATION_FUNCTION_INDEX'] = MAX_ACTIVATION_FUNCTION_INDEX = len(activation_functions) - 1
Global.CNN_CONFIG['MIN_ACTIVATION_FUNCTION_INDEX'] = MIN_ACTIVATION_FUNCTION_INDEX = 0

Global.CNN_CONFIG['MAX_POLLING_TYPE_INDEX'] = MAX_POLLING_TYPE_INDEX = len(polling_types) - 1
Global.CNN_CONFIG['MIN_POLLING_TYPE_INDEX'] = MIN_POLLING_TYPE_INDEX = 0

Global.CNN_CONFIG['MAX_DROPOUT_RATE'] = MAX_DROPOUT_RATE = 50 # IMPLICITNE DELENO 100
Global.CNN_CONFIG['MIN_DROPOUT_RATE'] = MIN_DROPOUT_RATE = 0   # IMPLICITNE DELENO 100

Global.CNN_CONFIG['MAX_UNITS_IN_DENSE_LAYER'] = MAX_UNITS_IN_DENSE_LAYER = 700
Global.CNN_CONFIG['MIN_UNITS_IN_DENSE_LAYER'] = MIN_UNITS_IN_DENSE_LAYER = 100

Global.CNN_CONFIG['MAX_CONVOLUTION_LAYERS'] = MAX_CONVOLUTION_LAYERS = 3
Global.CNN_CONFIG['MAX_DENSE_LAYER'] = MAX_DENSE_LAYER = 3

Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] = NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER = 5
Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_DENSE_LAYER'] = NUM_OF_ELEMENTS_FOR_DENSE_LAYER = 3

Global.CNN_CONFIG['NUM_OF_GENES_FOR_INDIVIDUAL'] = NUM_OF_GENES_FOR_INDIVIDUAL = MAX_CONVOLUTION_LAYERS * NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER + MAX_DENSE_LAYER * NUM_OF_ELEMENTS_FOR_DENSE_LAYER


# TEST WORST CASE

"""Global.CNN_CONFIG['activation_functions'] = activation_functions = [tf.nn.sigmoid] # Activation Functions: sigmoid, tanh, elu, softplus, and softsign, relu, relu6, crelu and relu_x, and random regularization (dropout).
Global.CNN_CONFIG['polling_types'] = polling_types = [tf.layers.average_pooling2d]

Global.CNN_CONFIG['MAX_FILTERS_NUM'] = MAX_FILTERS_NUM = 70
Global.CNN_CONFIG['MIN_FILTERS_NUM'] = MIN_FILTERS_NUM = 70

Global.CNN_CONFIG['MAX_FILTER_SIZE'] = MAX_FILTER_SIZE = 10
Global.CNN_CONFIG['MIN_FILTER_SIZE'] = MIN_FILTER_SIZE = 10

Global.CNN_CONFIG['MAX_ACTIVATION_FUNCTION_INDEX'] = MAX_ACTIVATION_FUNCTION_INDEX = 0
Global.CNN_CONFIG['MIN_ACTIVATION_FUNCTION_INDEX'] = MIN_ACTIVATION_FUNCTION_INDEX = 0

Global.CNN_CONFIG['MAX_POLLING_TYPE_INDEX'] = MAX_POLLING_TYPE_INDEX = 0
Global.CNN_CONFIG['MIN_POLLING_TYPE_INDEX'] = MIN_POLLING_TYPE_INDEX = 0

Global.CNN_CONFIG['MAX_DROPOUT_RATE'] = MAX_DROPOUT_RATE = 50 # IMPLICITNE DELENO 100
Global.CNN_CONFIG['MIN_DROPOUT_RATE'] = MIN_DROPOUT_RATE = 0   # IMPLICITNE DELENO 100

Global.CNN_CONFIG['MAX_UNITS_IN_DENSE_LAYER'] = MAX_UNITS_IN_DENSE_LAYER = 700
Global.CNN_CONFIG['MIN_UNITS_IN_DENSE_LAYER'] = MIN_UNITS_IN_DENSE_LAYER = 700

Global.CNN_CONFIG['MAX_CONVOLUTION_LAYERS'] = MAX_CONVOLUTION_LAYERS = 3
Global.CNN_CONFIG['MAX_DENSE_LAYER'] = MAX_DENSE_LAYER = 3

Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] = NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER = 5
Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_DENSE_LAYER'] = NUM_OF_ELEMENTS_FOR_DENSE_LAYER = 3

Global.CNN_CONFIG['NUM_OF_GENES_FOR_INDIVIDUAL'] = NUM_OF_GENES_FOR_INDIVIDUAL = MAX_CONVOLUTION_LAYERS * NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER + MAX_DENSE_LAYER * NUM_OF_ELEMENTS_FOR_DENSE_LAYER"""


tf.logging.set_verbosity(tf.logging.INFO)
tf.logging.set_verbosity(tf.logging.ERROR)

#TRAIN_IMAGES = 'train-images-idx3-ubyte.gz'
#TRAIN_LABELS = 'train-labels-idx1-ubyte.gz'
#TEST_IMAGES = 't10k-images-idx3-ubyte.gz'
#TEST_LABELS = 't10k-labels-idx1-ubyte.gz'

mnist = tf.contrib.learn.datasets.mnist.read_data_sets(
                              MNIST_DATA_DIR,
                              fake_data=False,
                              one_hot=False,
                              dtype=dtypes.float32,
                              reshape=True,
                              validation_size=5000,
                              seed=None)


train_data = mnist.train.images  # Returns np.array
train_labels = np.asarray(mnist.train.labels, dtype=np.int32)
eval_data = mnist.test.images  # Returns np.array
eval_labels = np.asarray(mnist.test.labels, dtype=np.int32)


# Část kódu převzata z tutoriálu o tom, jak postavit CNN v Tensorflow: https://www.tensorflow.org/tutorials/layers#build_a_multilayer_convolutional_network
class CNN(object):
    #             KONVOLUCNI VRSTVA                         |   MAX POOLING VRSTVA                | REPEAT | DENSE VRSTVA                                      | REPEAT
    #[is_active_convolution__layer, filters_num, filter_size, is_active_pooling_layer, polling_type,......., is_active_dense_layer, units, activation_function,  ......]
    def cnn_model_fn(self, features, labels, mode, params):
        individualGenome = params['individual']
        learning_rate = params['learning_rate']

        """Model function for CNN."""
        # Input Layer
        # Reshape X to 4-D tensor: [batch_size, width, height, channels]
        # MNIST images are 28x28 pixels, and have one color channel
        input_layer = tf.reshape(features["x"], [-1, 28, 28, 1])


        ################ KONVOLUCNI VRSTVY ####################
        offset = 0
        last_filter_num = individualGenome[offset]
        current_image_size = 28

        # write once per fitess evaluation (when train not evaluate)
        if mode == tf.estimator.ModeKeys.TRAIN:
            file2.write("%s\n" % individualGenome)
            file2.flush()
            os.fsync(file2.fileno())

        # z genotypu konvolucni verstvy vytvorime fenotyp
        for i in range(MAX_CONVOLUTION_LAYERS):
            is_active_convolution_layer = individualGenome[offset + NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER * i + 0]
            filters_num = individualGenome[offset + NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER * i + 1]
            filter_size = individualGenome[offset + NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER * i + 2]
            is_active_pooling_layer = individualGenome[offset + NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER * i + 3]
            polling_type_index = individualGenome[offset + NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER * i + 4]
            #is_active_dropout_layer = individualGenome[offset + 7 * i + 5]
            #dropout_rate = individualGenome[offset + 7 * i + 6]

            """file2.write("is_active_convolution_layer: %s | filters_num: %s | filter_size: %s | is_active_pooling_layer: %s | polling_type_index: %s \n" % (is_active_convolution_layer, filters_num, filter_size, is_active_pooling_layer, polling_type_index))
            file2.flush()
            os.fsync(file2.fileno())"""

            if is_active_convolution_layer:
                input_layer = tf.layers.conv2d(
                  inputs=input_layer,
                  filters=filters_num,
                  kernel_size=[filter_size, filter_size],
                  padding="same",
                  activation=tf.nn.relu)

                if is_active_pooling_layer:
                    input_layer = polling_types[polling_type_index](inputs=input_layer, pool_size=[2, 2], strides=2)
                    # po polingu si prubezne pocitame rozmery obarzku
                    if (current_image_size % 2 == 0):
                        current_image_size = current_image_size // 2
                    else:
                        current_image_size = current_image_size // 2

                #if is_active_dropout_layer:
                #    input_layer = tf.layers.dropout(inputs=input_layer, rate=dropout_rate / 100, training=mode == tf.estimator.ModeKeys.TRAIN)
                input_layer = tf.layers.dropout(inputs=input_layer, rate= 10 / 100, training=mode == tf.estimator.ModeKeys.TRAIN)

            last_filter_num = filters_num
        ############# FLATTEN TENSOR ###############

        # Flatten tensor into a batch of vectors
        # Input Tensor Shape: [batch_size, 7, 7, 64]
        # Output Tensor Shape: [batch_size, 7 * 7 * 64]
        #print(input_layer.get_shape())
        sum = 1
        for i in input_layer.get_shape().as_list()[1:]:
            sum *= i

        #print([-1, sum])

        input_layer = tf.reshape(input_layer, [-1, sum])

        ############# DENSE VRSTVY ###############

        offset += MAX_CONVOLUTION_LAYERS * NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER
        # z genotypu dense vrstvy vytvorime fenotyp
        for i in range(MAX_DENSE_LAYER):
            is_active_dense_layer = individualGenome[offset + NUM_OF_ELEMENTS_FOR_DENSE_LAYER * i + 0]
            units = individualGenome[offset + NUM_OF_ELEMENTS_FOR_DENSE_LAYER * i + 1]
            activation_function_index = individualGenome[offset + NUM_OF_ELEMENTS_FOR_DENSE_LAYER * i + 2]
            #is_active_dropout_layer = individualGenome[offset + 5 * i + 3]
            #dropout_rate = individualGenome[offset + 5 * i + 4]

            """file2.write("is_active_dense_layer: %s | units: %s | activation_function_index: %s \n" % (is_active_dense_layer, units, activation_function_index))
            file2.flush()
            os.fsync(file2.fileno())"""

            if is_active_dense_layer:
                input_layer = tf.layers.dense(inputs=input_layer, units=units, activation=activation_functions[activation_function_index])

                #if is_active_dropout_layer:
                #    input_layer = tf.layers.dropout(inputs=input_layer, rate=dropout_rate / 100, training=mode == tf.estimator.ModeKeys.TRAIN)
            input_layer = tf.layers.dropout(inputs=input_layer, rate= 20 / 100, training=mode == tf.estimator.ModeKeys.TRAIN)


        ################# OUTPUT VRSTVA #########################X

        # Logits layer
        # Input Tensor Shape: [batch_size, 1024]
        # Output Tensor Shape: [batch_size, 10]
        input_layer = tf.layers.dense(inputs=input_layer, units=10)

        predictions = {
            # Generate predictions (for PREDICT and EVAL mode)
            "classes": tf.argmax(input=input_layer, axis=1),
            # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
            # `logging_hook`.
            "probabilities": tf.nn.softmax(input_layer, name="softmax_tensor")
        }
        if mode == tf.estimator.ModeKeys.PREDICT:
            return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

        # Calculate Loss (for both TRAIN and EVAL modes)
        onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=10)
        loss = tf.losses.softmax_cross_entropy(onehot_labels=onehot_labels, logits=input_layer)

        # Configure the Training Op (for TRAIN mode)
        if mode == tf.estimator.ModeKeys.TRAIN:
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
            train_op = optimizer.minimize(
                loss=loss,
                global_step=tf.train.get_global_step())
            return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

        # Add evaluation metrics (for EVAL mode)
        eval_metric_ops = {
          "accuracy": tf.metrics.accuracy(
              labels=labels, predictions=predictions["classes"])}
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

    def getCNNFitness(self, model_params_in_dictionary):

        try:
            tf.gfile.DeleteRecursively(MODEL_PATH)
        except Exception:
            pass

        # Create the Estimator
        run_config = tf.estimator.RunConfig()
        run_config = run_config.replace(tf_random_seed=0)
        #run_config = run_config.replace(save_checkpoints_steps=None)
        run_config = run_config.replace(save_checkpoints_secs=5000)
        mnist_classifier = tf.estimator.Estimator(
            model_fn=self.cnn_model_fn, model_dir=MODEL_PATH,
            config=run_config, params=model_params_in_dictionary)

        # Set up logging for predictions
        # Log the values in the "Softmax" tensor with label "probabilities"
        tensors_to_log = {"probabilities": "softmax_tensor"}
        logging_hook = tf.train.LoggingTensorHook(
            tensors=tensors_to_log, every_n_iter=100000)

        # Train the model
        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": train_data},
            y=train_labels,
            batch_size=100,
            num_epochs=None,
            shuffle=False)
        mnist_classifier.train(
            input_fn=train_input_fn,
            steps=50,
            hooks=[])  # hooks=[logging_hook])

        # Evaluate the model and print results
        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": eval_data},
            y=eval_labels,
            num_epochs=1,
            shuffle=False)
        eval_results = mnist_classifier.evaluate(input_fn=eval_input_fn)

        return eval_results

    def createAndSaveCNNModelFromIndividual(self, individual, save_location):
        model_params_in_dictionary = {
            'individual': individual,
            'learning_rate': Global.CONFIG['LEARNING_RATE']
        }

        ##################### START FITNESS EVALUATION ################################
        # Create the Estimator
        run_config = tf.estimator.RunConfig()
        run_config = run_config.replace(tf_random_seed=0)
        #run_config = run_config.replace(save_checkpoints_steps=None)
        #run_config = run_config.replace(save_checkpoints_secs=None)
        mnist_classifier = tf.estimator.Estimator(
            model_fn=self.cnn_model_fn, model_dir=save_location,
            config=run_config, params=model_params_in_dictionary)

        # Train the model
        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": train_data},
            y=train_labels,
            batch_size=1,
            num_epochs=None,
            shuffle=False)
        mnist_classifier.train(
            input_fn=train_input_fn,
            steps=1,
            hooks=[])  # hooks=[logging_hook])

        return mnist_classifier

    def format_print_individual(self, individual):
        output_str = ""
        first_dense_layer_index = MAX_CONVOLUTION_LAYERS * NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER
        for i in range(NUM_OF_GENES_FOR_INDIVIDUAL):
            if i < first_dense_layer_index:
                if ((i + 1) % NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER) == 0:
                    output_str += str(individual[i]) + "\n"
                else:
                    output_str += str(individual[i]) + " , "
            else:
                if i == first_dense_layer_index:
                    output_str += "----------\n"
                if (((i + 1) - first_dense_layer_index) % NUM_OF_ELEMENTS_FOR_DENSE_LAYER) == 0:
                    output_str += str(individual[i]) + "\n"
                else:
                    output_str += str(individual[i]) + " , "

        return output_str

    def testIndividual(self, individual, individual_decription, model_outdir=MODEL_PATH):
        model_params_in_dictionary = {
            'individual': individual,
            'learning_rate': 0.05
        }

        try:
            tf.gfile.DeleteRecursively(model_outdir)
        except Exception:
            pass

        # Do the work here
        start = MPI.Wtime()

        ##################### START FITNESS EVALUATION ################################
        # Create the Estimator
        run_config = tf.estimator.RunConfig()
        run_config = run_config.replace(tf_random_seed=0)
        run_config = run_config.replace(save_checkpoints_secs=5*3600)
        run_config = run_config.replace(keep_checkpoint_max=1)

        mnist_classifier = tf.estimator.Estimator(
            model_fn=self.cnn_model_fn, model_dir=model_outdir,
            config=run_config, params=model_params_in_dictionary)

        # Set up logging for predictions
        # Log the values in the "Softmax" tensor with label "probabilities"
        tensors_to_log = {"probabilities": "softmax_tensor"}
        logging_hook = tf.train.LoggingTensorHook(
            tensors=tensors_to_log, every_n_iter=100000)

        # Train the model
        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": train_data},
            y=train_labels,
            batch_size=100,
            num_epochs=50,
            shuffle=False)
        mnist_classifier.train(
            input_fn=train_input_fn,
            steps=None,
            hooks=[])  # hooks=[logging_hook])

        # Evaluate the model and print results
        eval_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": eval_data},
            y=eval_labels,
            num_epochs=1,
            shuffle=False)
        eval_results = mnist_classifier.evaluate(input_fn=eval_input_fn)

        ##################### STOP FITNESS EVALUATION ################################
        end = MPI.Wtime()

        file_common = open(OUTPUT_DIR_NAME + "/../out-result.txt", "a")
        file_common.write("%s | Rank: %s | Individual: %s\n" % (individual_decription, rank, individual))
        file_common.write("%s | Rank: %s | Time elapsed: %s\n" % (individual_decription, rank, end - start))
        file_common.write("%s | Rank: %s | Results: %s\n\n" % (individual_decription, rank, eval_results))
        file_common.flush()
        os.fsync(file_common.fileno())
        file_common.close()

        file2.write("%s | Rank: %s | Individual: %s\n" % (individual_decription, rank, individual))
        file2.write("%s | Rank: %s | Time elapsed: %s\n" % (individual_decription, rank, end - start))
        file2.write("%s | Rank: %s | Results: %s\n" % (individual_decription, rank, eval_results))
        file2.flush()
        os.fsync(file2.fileno())
        file2.close()

    def testIndividuals(self):
        model_outdir = Global.CONFIG['OUTPUT_MODEL_DIR_NAME'] + "/" + str(rank)
        individuals = [
            [0, 53, 6, 1, 0, 1, 8, 6, 0, 1, 1, 30, 9, 1, 1, 1, 269, 2, 1, 302, 2, 1, 111, 2],    # GA-var1
            [1, 70, 6, 0, 0, 0, 25, 8, 1, 0, 1, 57, 9, 1, 1, 1, 673, 2, 1, 551, 2, 0, 535, 1],   # GA-var2
            [1, 5, 2, 0, 0, 1, 45, 6, 1, 1, 1, 36, 8, 1, 1, 1, 489, 2, 0, 349, 1, 0, 199, 1],    # GA-var3
            [1, 5, 2, 0, 0, 1, 22, 5, 1, 1, 1, 37, 9, 1, 1, 0, 151, 0, 0, 622, 1, 1, 193, 2],    # GA-var4
            [1, 31, 2, 0, 1, 1, 45, 8, 1, 1, 1, 64, 9, 1, 1, 0, 297, 2, 1, 672, 2, 1, 176, 2],   # GA-var5
            [1, 25, 9, 0, 1, 1, 47, 9, 1, 1, 0, 51, 2, 1, 1, 1, 457, 2, 1, 301, 2, 0, 475, 1],   # GA-var6

            [1, 62, 9, 0, 0, 0, 48, 7, 1, 0, 1, 34, 7, 1, 1, 0, 282, 0, 1, 567, 2, 1, 288, 2],   # ES-var1
            [1, 11, 5, 0, 1, 1, 34, 10, 0, 1, 0, 32, 6, 1, 1, 1, 542, 2, 0, 258, 2, 1, 484, 2],  # ES-var2
            [0, 20, 3, 1, 0, 1, 11, 5, 0, 1, 1, 27, 8, 1, 1, 1, 533, 2, 0, 620, 1, 1, 170, 2],   # ES-var3
            [1, 25, 2, 0, 0, 1, 54, 8, 1, 1, 1, 46, 8, 1, 1, 0, 130, 1, 0, 558, 2, 1, 470, 2],   # ES-var4
            [1, 70, 6, 0, 0, 0, 5, 4, 0, 0, 1, 49, 9, 1, 1, 1, 400, 2, 1, 694, 2, 1, 586, 2],    # ES-var5
            [1, 59, 5, 0, 0, 1, 60, 8, 1, 1, 0, 55, 9, 1, 1, 0, 446, 1, 1, 489, 2, 0, 399, 0]    # ES-var6

        ]
        individuals_decription = [
            "GA-var1", "GA-var2", "GA-var3", "GA-var4", "GA-var5", "GA-var6",
            "ES-var1", "ES-var2", "ES-var3", "ES-var4", "ES-var5", "ES-var6"
        ]
        if rank < len(individuals):
            os.makedirs(model_outdir, exist_ok=True)
            self.testIndividual(individuals[rank], individual_decription=individuals_decription[rank], model_outdir=model_outdir)
        comm.Barrier()
        sys.exit()

def main(unused_argv):

    cnn = CNN()
    cnn.testIndividuals()
    #cnn.testIndividual([1, 26, 6, 0, 1, 0, 58, 7, 1, 1, 0, 23, 3, 0, 0, 0, 41, 3, 0, 0, 0, 136, 0, 0, 148, 2, 1, 54, 2, 0, 649, 2])

    # proces s rankem 0 bude provádět evoluční algoritmus a jen vyhodnocení fitness hodnoty pro jedince si nechá paralelně spočítat oatstaními prcesy
    if rank == 0:
        print(str(Global.EA_CONFIG))
        print(str(Global.CONFIG))
        print(str(Global.CNN_CONFIG))

        individual = np.ones(NUM_OF_GENES_FOR_INDIVIDUAL, dtype='i')  # zakodovany jedinec se samimy jednockami, posilame jej na konci vypoctu vsem procesum s tagem exit abychom jim dali vedet ze vypocet skoncil
        start = MPI.Wtime()

        if Global.EA_CONFIG['USED_EA'] == EvolutionProcess.GA:
            evolution = EvolutionProcessGA(cnn)
        elif Global.EA_CONFIG['USED_EA'] == EvolutionProcess.ES:
            evolution = EvolutionProcessES(cnn)

        evolution.evolve() # do evolution

        # tel other processes taht evolution existed
        list_of_requests = []
        for i in range(Global.EA_CONFIG['DEFAULT_POP_SIZE']):
            req = comm.Isend([individual, MPI.INT], dest=i + 1, tag=Global.TAG_EXIT)
            list_of_requests.append(req)

        MPI.Request.Waitall(list_of_requests)

        end = MPI.Wtime()
        print("Evolution time elapsed: " + str(end - start))
        file2.write("Evolution time elapsed: " + str(end - start) + "\n")
        file2.flush()
        os.fsync(file2.fileno())
        file2.close()

        file_common.write("IDP_RUN: %s : OK \n" % Global.CONFIG['INDEPENDENT_RUN'])
        file_common.flush()
        os.fsync(file_common.fileno())
        file_common.close()


    # pocesy mimo rank 0 budou počítat paralelne fitness
    else:
        individual = np.empty(NUM_OF_GENES_FOR_INDIVIDUAL, dtype='i') # zakodovany jedinec ktereo prijmeme od procesu s rankem 0
        individual_fitness = np.empty(1, dtype='f8')

        '''try:
            shutil.rmtree(MODEL_PATH)
        except Exception:
            pass'''
        try:
            tf.gfile.DeleteRecursively(MODEL_PATH)
        except Exception:
            pass

        # dokud nedostaneme zprávu od procesu s rankem 0, že evoluce skončila, tak očekáváme k vyhodnicení fitness
        while True:
            comm.Recv([individual, MPI.INT], source=0, tag=MPI.ANY_TAG, status=status)
            model_params_in_dictionary = {
                'individual': individual,
                'learning_rate': Global.CONFIG['LEARNING_RATE']
            }
            tag = status.Get_tag()
            # vyhodnotime fitness jedince
            if tag == Global.TAG_FITNESS_EVALUATE:
                # Do the work here
                start = MPI.Wtime()

                ##################### START FITNESS EVALUATION ################################
                eval_results = cnn.getCNNFitness(model_params_in_dictionary)
                ##################### STOP FITNESS EVALUATION ################################

                end = MPI.Wtime()
                file2.write("time elapsed: " + str(end - start) + ", " + str(eval_results) + "\n")
                file2.flush()
                os.fsync(file2.fileno())


                individual_fitness[0] = eval_results['accuracy']
                comm.Send([individual_fitness, MPI.DOUBLE], dest=0, tag=Global.TAG_FITNESS_RETURNED)

                '''try:
                    shutil.rmtree(MODEL_PATH)
                except Exception:
                    pass'''
            elif tag == Global.TAG_EXIT:
                break

        file2.close()

if __name__ == "__main__":
  tf.app.run()
