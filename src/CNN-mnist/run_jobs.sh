#!/bin/bash
#module load Bash/4.4-intel-2017b 
#module load zsh/5.2-foss-2018a
output_dir="/scratch/work/user/xherec00/cnn/outputs/final/experiment2-full-training/50-5"
num_independent_runs=1
default_pop_size=23
rm -rf ${output_dir}/

mkdir -p ${output_dir}
cp /home/xherec00/tensorflow/cnn/cnn_mnist7.py /home/xherec00/tensorflow/cnn/evolution_algorithms.py /home/xherec00/tensorflow/cnn/EvolutionStatistics.py /home/xherec00/tensorflow/cnn/globb.py ${output_dir}

#unset module
seed_time=$(date +%s)
for i in `seq -w $1`
do
	for j in `seq -w $num_independent_runs`
	do
		seed_time=$(($seed_time + 1))
		echo ${seed_time}
		qsub -v evolution_run_number=${i},output_dir=${output_dir},seed=${seed_time},independent_run=${j},num_independent_runs=${num_independent_runs},default_pop_size=${default_pop_size} -N evolution_run_number_${i} run.pbs
	done
done
