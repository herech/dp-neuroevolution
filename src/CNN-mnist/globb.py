class Global(object):
    CONFIG = {}
    EA_CONFIG = {}
    CNN_CONFIG = {}

    # GLOBAL CONSTANTS
    TAG_FITNESS_EVALUATE = 0
    TAG_FITNESS_RETURNED = 1
    TAG_EXIT = 2
