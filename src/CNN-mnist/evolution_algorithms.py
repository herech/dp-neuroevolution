import argparse
import sys
import numpy as np

from scipy.optimize import differential_evolution

import gym
from gym import wrappers, logger

from keras.models import Sequential
from keras.layers import Dense
from keras import backend as K

from keras.utils import plot_model
from keras.layers.advanced_activations import LeakyReLU

from deap import base
from deap import creator
from deap import tools

import random
import time

from mpi4py import MPI
import h5py

import os
import shutil

import math

import EvolutionStatistics
from globb import Global

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()
status = MPI.Status()   # get MPI status object

random_seed = int(os.environ['SEED'])
random.seed(random_seed) # nastavime seed

if rank == 0:
    logfile = open(str(os.environ['NEUROEVOLUTION_OUTPUT_DIR']) + "/../out-fitneses.txt", "a")

if rank == 0:
    file2 = open(str(os.environ['NEUROEVOLUTION_OUTPUT_DIR']) + "/../out-" + str(rank) + ".txt", "a")

class EvolutionProcess(object):
    GA = 1
    ES = 2

    def __init__(self, cnn):
        self.cnn = cnn
        self.evolution_statistics = EvolutionStatistics.EvolutionStatistics(Global.CONFIG['OUTPUT_DIR_NAME'] + "/..")

        creator.create("FitnessMax", base.Fitness, weights=(1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMax)

        self.toolbox = base.Toolbox()

        self.toolbox.register("individual", self.individualInit, container=creator.Individual)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)

        # Operator registering
        self.toolbox.register("mutate", self.mutation, indpb=Global.EA_CONFIG['INDPB'])

        self._create_record_of_evolution()

    def save_plot_evolution_statistics(self):
        self.evolution_statistics.load_record_of_evolution()
        self.evolution_statistics.plot_record()

    #             KONVOLUCNI VRSTVA                         |   MAX POOLING VRSTVA                | REPEAT | DENSE VRSTVA                                      | REPEAT
    # [is_active_convolution__layer, filters_num, filter_size, is_active_pooling_layer, polling_type,......., is_active_dense_layer, units, activation_function,  ......]
    def individualInit(self, container):
        individualGenome = []
        # vytvorime konvolucni vrstvy
        for i in range(Global.CNN_CONFIG['MAX_CONVOLUTION_LAYERS']):
            individualGenome.append(random.randint(0, 1))
            individualGenome.append(random.randint(Global.CNN_CONFIG['MIN_FILTERS_NUM'], Global.CNN_CONFIG['MAX_FILTERS_NUM']))
            individualGenome.append(random.randint(Global.CNN_CONFIG['MIN_FILTER_SIZE'], Global.CNN_CONFIG['MAX_FILTER_SIZE']))
            individualGenome.append(random.randint(0, 1))
            individualGenome.append(random.randint(Global.CNN_CONFIG['MIN_POLLING_TYPE_INDEX'], Global.CNN_CONFIG['MAX_POLLING_TYPE_INDEX']))
            # individualGenome.append(random.randint(0, 1))
            # individualGenome.append(random.randint(Global.CNN_CONFIG['MIN_DROPOUT_RATE'], Global.CNN_CONFIG['MAX_DROPOUT_RATE']))

        # vytvorime dense vrstvy
        for i in range(Global.CNN_CONFIG['MAX_DENSE_LAYER']):
            individualGenome.append(random.randint(0, 1))
            individualGenome.append(random.randint(Global.CNN_CONFIG['MIN_UNITS_IN_DENSE_LAYER'], Global.CNN_CONFIG['MAX_UNITS_IN_DENSE_LAYER']))
            individualGenome.append(random.randint(Global.CNN_CONFIG['MIN_ACTIVATION_FUNCTION_INDEX'], Global.CNN_CONFIG['MAX_ACTIVATION_FUNCTION_INDEX']))
            # individualGenome.append(random.randint(0, 1))
            # individualGenome.append(random.randint(Global.CNN_CONFIG['MIN_DROPOUT_RATE'], Global.CNN_CONFIG['MAX_DROPOUT_RATE']))

        return container(individualGenome)

    def fitnessPopulationEvaluate(self, pop):
        individual_fitness = np.empty(1, dtype='f8')

        if len(pop) == Global.EA_CONFIG['DEFAULT_POP_SIZE']:
            list_of_requests = []
            for i in range(len(pop)):
                individual = np.ascontiguousarray(pop[i], dtype='i')
                req = comm.Isend([individual, MPI.INT], dest=i + 1, tag=Global.TAG_FITNESS_EVALUATE)
                list_of_requests.append(req)

            MPI.Request.Waitall(list_of_requests)
            # for i in range(len(indexes_to_evaluate_fitness)):
            #    list_of_requests[i].wait()

            for i in range(len(pop)):
                comm.Recv([individual_fitness, MPI.DOUBLE], source=MPI.ANY_SOURCE, tag=Global.TAG_FITNESS_RETURNED, status=status)
                source = status.Get_source()

                pop[source - 1].fitness.values = (individual_fitness[0],) # must be tuple because of multiobjective optimalization

        else:
            num_serial_evals=len(pop) // Global.EA_CONFIG['DEFAULT_POP_SIZE']
            for batch in range(num_serial_evals):
                list_of_requests = []
                for i in range(Global.EA_CONFIG['DEFAULT_POP_SIZE']):
                    individual = np.ascontiguousarray(pop[i + Global.EA_CONFIG['DEFAULT_POP_SIZE'] * batch], dtype='i')
                    req = comm.Isend([individual, MPI.INT], dest=i + 1, tag=Global.TAG_FITNESS_EVALUATE)
                    list_of_requests.append(req)

                MPI.Request.Waitall(list_of_requests)
                # for i in range(len(indexes_to_evaluate_fitness)):
                #    list_of_requests[i].wait()

                for i in range(Global.EA_CONFIG['DEFAULT_POP_SIZE']):
                    comm.Recv([individual_fitness, MPI.DOUBLE], source=MPI.ANY_SOURCE, tag=Global.TAG_FITNESS_RETURNED, status=status)
                    source = status.Get_source()

                    pop[source - 1 + Global.EA_CONFIG['DEFAULT_POP_SIZE'] * batch].fitness.values = (individual_fitness[0],)  # must be tuple because of multiobjective optimalization


    def mutation(self, individualGenome, indpb):
        offset = 0

        # zmutujeme konvolucni vrstvy
        for i in range(Global.CNN_CONFIG['MAX_CONVOLUTION_LAYERS']):
            if random.random() < indpb:
                individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] * i + 0] = random.randint(0, 1)
            if random.random() < indpb:
                individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] * i + 1] = random.randint(Global.CNN_CONFIG['MIN_FILTERS_NUM'], Global.CNN_CONFIG['MAX_FILTERS_NUM'])
            if random.random() < indpb:
                individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] * i + 2] = random.randint(Global.CNN_CONFIG['MIN_FILTER_SIZE'], Global.CNN_CONFIG['MAX_FILTER_SIZE'])
            if random.random() < indpb:
                individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] * i + 3] = random.randint(0, 1)
            if random.random() < indpb:
                individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] * i + 4] = random.randint(Global.CNN_CONFIG['MIN_POLLING_TYPE_INDEX'], Global.CNN_CONFIG['MAX_POLLING_TYPE_INDEX'])
            # if random.random() < indpb * 1.2:
            #    individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] * i + 5] = random.randint(0, 1)
            # if random.random() < indpb:
            #   individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER'] * i + 6] = random.randint(Global.CNN_CONFIG['MIN_DROPOUT_RATE'], Global.CNN_CONFIG['MAX_DROPOUT_RATE'])

        offset += Global.CNN_CONFIG['MAX_CONVOLUTION_LAYERS'] * Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_CONVOLUTION_LAYER']
        # zmutujeme dense vrstvy
        for i in range(Global.CNN_CONFIG['MAX_DENSE_LAYER']):
            if random.random() < indpb:
                individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_DENSE_LAYER'] * i + 0] = random.randint(0, 1)
            if random.random() < indpb:
                individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_DENSE_LAYER'] * i + 1] = random.randint(Global.CNN_CONFIG['MIN_UNITS_IN_DENSE_LAYER'], Global.CNN_CONFIG['MAX_UNITS_IN_DENSE_LAYER'])
            if random.random() < indpb:
                individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_DENSE_LAYER'] * i + 2] = random.randint(Global.CNN_CONFIG['MIN_ACTIVATION_FUNCTION_INDEX'], Global.CNN_CONFIG['MAX_ACTIVATION_FUNCTION_INDEX'])
            # if random.random() < indpb * 1.2:
            #    individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_DENSE_LAYER'] * i + 3] = random.randint(0, 1)
            # if random.random() < indpb:
            #    individualGenome[offset + Global.CNN_CONFIG['NUM_OF_ELEMENTS_FOR_DENSE_LAYER'] * i + 4] = random.randint(Global.CNN_CONFIG['MIN_DROPOUT_RATE'], Global.CNN_CONFIG['MAX_DROPOUT_RATE'])


    def _create_record_of_evolution(self):
        if Global.EA_CONFIG['USED_EA'] == EvolutionProcess.GA:
            popsize = Global.EA_CONFIG['POPULATION_SIZE']
            num_evaluations_per_generation = Global.EA_CONFIG['POPULATION_SIZE']
            evolution_algorithm = "GA"
        elif Global.EA_CONFIG['USED_EA'] == EvolutionProcess.ES:
            popsize = Global.EA_CONFIG['POPULATION_SIZE']
            num_evaluations_per_generation = Global.EA_CONFIG['ES_NUM_CHILDREN']
            evolution_algorithm = "ES"

        self.evolution_statistics.create_record_of_evolution(rank=Global.CONFIG['INDEPENDENT_RUN'] - 1, num_generations=Global.EA_CONFIG['NGEN'], popsize=popsize, num_independen_runs=Global.CONFIG['NUM_INDEPENDENT_RUNS'], num_evaluations_per_generation=num_evaluations_per_generation, num_of_records=Global.CONFIG['NUM_RECORDS'], evolution_algorithm = evolution_algorithm, evolution_algorithm_settings=Global.EA_CONFIG, other_settings=Global.CONFIG, cnn_settings=Global.CNN_CONFIG)

    def _evolution_start(self):

        if Global.CONFIG['INDEPENDENT_RUN'] == 1:
            file2.write("%s\n" % str(Global.EA_CONFIG))
            file2.write("%s\n" % str(Global.CONFIG))
            file2.write("%s\n" % str(Global.CNN_CONFIG))

        if rank == 0:
            print("Start of evolution with random seed :%s|%s" % (Global.CONFIG['SEED'], random_seed))
        file2.write("Start of evolution with random seed :%s|%s\n" % (Global.CONFIG['SEED'], random_seed))

        self.start_time = MPI.Wtime()

        pop = self.toolbox.population(n=Global.EA_CONFIG['POPULATION_SIZE'])

        # Evaluate the entire population
        self.fitnessPopulationEvaluate(pop)
        #print("  Evaluated %i individuals" % len(pop))
        #file2.write("Evaluated %i individuals\n" % len(pop))
        file2.flush()
        os.fsync(file2.fileno())

        return pop

    def _generation_start(self, g):

        self.start_start_time = MPI.Wtime()
        if rank == 0:
            print("-- Generation %i --" % g)
        file2.write("-- Generation %i --\n" % g)

    def _generation_finished(self, pop, g):

        save_progress = g % (Global.EA_CONFIG['NGEN'] // Global.CONFIG['NUM_SAVED_PROGRESS']) == 0 or g == 1 # flag if save progress
        save_evolution_record = g % (Global.EA_CONFIG['NGEN'] // Global.CONFIG['NUM_RECORDS']) == 0 or g == 1 # flag if save evolution record
        save_model = g % (Global.EA_CONFIG['NGEN'] // Global.CONFIG['NUM_MODELS']) == 0
        # if not special flag we should save video from 1. generation
        if Global.CONFIG['RENDER_MODEL_FOR_FIRST_GENERATION']:
            save_model = save_model or g == 1

        if save_progress or save_evolution_record or save_model:

            # Gather all the fitnesses in one list and print the stats
            fits = [ind.fitness.values[0] for ind in pop]

            length = len(pop)
            mean = sum(fits) / length
            sum2 = sum(x * x for x in fits)
            std = abs(sum2 / length - mean ** 2) ** 0.5
            min_fit = min(fits)
            max_fit = max(fits)

            if save_progress:
                file2.write("G: %s | Min: %s | Max: %s | Avg: %s | Std: %s | Rank: %s\n" % (g, min_fit, max_fit, mean, std, Global.CONFIG['INDEPENDENT_RUN']))
                end = MPI.Wtime()
                file2.write("time elapsed in round/total: %s/%s\n" % (str(end - self.start_start_time), str(end - self.start_time)))

            if save_evolution_record:
                self.evolution_statistics.append_data_to_record(fitness_values=fits,
                                                                generation=g)


                logfile.write("G: %s | Min: %s | Max: %s | Avg: %s | Std: %s | Rank: %s\n" % (g, min_fit, max_fit, mean, std, Global.CONFIG['INDEPENDENT_RUN']))
                logfile.flush()
                os.fsync(logfile.fileno())

            #  behem evoluce nekolikrat ulozime model cnn
            if save_model:
                best_ind = tools.selBest(pop, 1)[0]
                worst_ind = tools.selWorst(pop, 1)[0]

                if g == Global.EA_CONFIG['NGEN']:
                    file2.write("G: %s, Rank: %s, Best individual is:\n %s, %s\n" % (g, Global.CONFIG['INDEPENDENT_RUN'], best_ind, best_ind.fitness.values))
                else:
                    file2.write("G: %s, Rank: %s, Best individual fitness is: %s\n" % (g, Global.CONFIG['INDEPENDENT_RUN'], best_ind.fitness.values))
                file2.flush()
                os.fsync(file2.fileno())

                # save cnn model and delete other created unnecesary files
                outdir = Global.CONFIG['OUTPUT_MODEL_DIR_NAME'] + "/" + str(g)
                os.makedirs(outdir, exist_ok=True)
                self.cnn.createAndSaveCNNModelFromIndividual(best_ind, outdir)

                outdir_content = os.listdir(outdir)

                for item in outdir_content:
                    if not item.endswith(".meta"):
                        os.remove(os.path.join(outdir, item))

                # save cnn genotype
                with open(Global.CONFIG['OUTPUT_INDIVIDUALS_IN_MODEL_DIR_NAME'] + "/individual-" + str(g), 'w') as individual_chromozome_file:
                    individual_chromozome_file.write("%s" % best_ind)

                # self.agent.importWeightsToModel(worst_ind)
                # self.simulator.simulation(episodes=1, record=True)


    def _evolution_finished(self):

        if rank == 0:
            print("-- End of (successful) evolution --")

        file2.write("-- End of (successful) evolution --\n")

        end = MPI.Wtime()
        file2.write("time elapsed: " + str(end - self.start_time) + "\n")
        file2.flush()
        os.fsync(file2.fileno())

        logfile.close()
        file2.close()


class EvolutionProcessGA(EvolutionProcess):

    CONFIG = {}

    def __init__(self, cnn):
        super().__init__(cnn)

        # Operator registering
        if Global.EA_CONFIG['CROSS_UNIFORM']:
            self.toolbox.register("mate", tools.cxUniform, indpb=Global.EA_CONFIG['INDPB_UNIFORM_CROSSOVER'])
        else:
            self.toolbox.register("mate", tools.cxTwoPoint)

        self.toolbox.register("select", tools.selTournament, tournsize=Global.EA_CONFIG['TOURNAMENT_SIZE'])


    def evolve(self):

        pop = self._evolution_start()

        # Begin the evolution
        for g in range(1, Global.EA_CONFIG['NGEN'] + 1):

            self._generation_start(g)

            # Select the next generation individuals
            offspring = self.toolbox.select(pop, len(pop) - 1)  # len(pop) - 1 = elitism | len(pop) = not elitism
            # Clone the selected individuals
            offspring = [self.toolbox.clone(tools.selBest(pop, 1)[0])] + list(map(self.toolbox.clone, offspring))
            #           [..............elitism..................]

            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[1::2], offspring[2::2]):  # [1::2] and [2::2] = elitism, first is elite | [::2] and [1::2] = not elitism
                if Global.EA_CONFIG['CROSS_ENABLED']:
                    if random.random() < Global.EA_CONFIG['CXPB']:
                        self.toolbox.mate(child1, child2)
                        del child1.fitness.values
                        del child2.fitness.values

            # apply mutation, elitism = offspring[1:] | not elitism = offspring
            for mutant in offspring[1:]:
                if random.random() < Global.EA_CONFIG['MUTPB']:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values


            # Evaluate the individuals with an invalid fitness
            #invalid_indexes_of_individual = [i for i in range(len(offspring)) if not offspring[i].fitness.valid]
            self.fitnessPopulationEvaluate(offspring)

            #print("  Evaluated %i individuals" % len(invalid_indexes_of_individual))
            #file2.write("Evaluated %i individuals\n" % len(invalid_indexes_of_individual))

            # The population is entirely replaced by the offspring
            pop[:] = offspring

            self._generation_finished(pop, g)

        self._evolution_finished()


class EvolutionProcessES(EvolutionProcess):

    CONFIG = {}

    def __init__(self, cnn):
        super().__init__(cnn)

        # Operator registering
        self.toolbox.register("mate", self.uniformCross, indpb=Global.EA_CONFIG['ES_INDPB_UNIFORM_CROSSOVER'])
        self.toolbox.register("select", tools.selRandom)

    def uniformCross(self, ind1, ind2, indpb):
        # cross solution vector
        for gene in range(len(ind1)):
            if random.random() < indpb:
                tmp_gene = ind1[gene]
                ind1[gene] = ind2[gene]
                ind2[gene] = tmp_gene

    def evolve(self):

        pop = self._evolution_start()

        # Begin the evolution
        for g in range(1, Global.EA_CONFIG['NGEN'] + 1):

            self._generation_start(g)

            # Select the next generation individuals
            offspring = self.toolbox.select(pop, Global.EA_CONFIG['ES_NUM_CHILDREN'])  # len(pop) - 1 = elitism | len(pop) = not elitism
            # Clone the selected individuals
            offspring = list(map(self.toolbox.clone, offspring))

            # Apply crossover and mutation on the offspring
            if Global.EA_CONFIG['CROSS_ES_ENABLED']:
                for child1, child2 in zip(offspring[::2], offspring[1::2]):  # [1::2] and [2::2] = elitism, first is elite | [::2] and [1::2] = not elitism
                    if random.random() < Global.EA_CONFIG['ES_CXPB']:
                        self.toolbox.mate(child1, child2)

            # apply mutation only on liche offspringy protoze jenom v nich je provedeno aritmeticke krizeni
            for mutant in offspring: #for mutant in offspring[0::2]:
                self.toolbox.mutate(mutant)

            # Evaluate the individuals
            self.fitnessPopulationEvaluate(offspring) # self.fitnessPopulationEvaluate(offspring[0::2], None)

            # The population is replaced by best of parents a and childs
            if (Global.EA_CONFIG['ES_COMMA_STRATEGY']):
                # comma startegy + elitism
                pop[:] = tools.selBest(offspring + [self.toolbox.clone(tools.selBest(pop, 1)[0])], Global.EA_CONFIG['POPULATION_SIZE']) #pop[:] = tools.selBest(offspring[0::2] + list(map(self.toolbox.clone, pop)), Global.EA_CONFIG['POPULATION_SIZE'])
            # plus startegy
            else:
                pop[:] = tools.selBest(offspring + list(map(self.toolbox.clone, pop)), Global.EA_CONFIG['POPULATION_SIZE']) #pop[:] = tools.selBest(offspring[0::2] + list(map(self.toolbox.clone, pop)), Global.EA_CONFIG['POPULATION_SIZE'])

            self._generation_finished(pop, g)

        self._evolution_finished()




